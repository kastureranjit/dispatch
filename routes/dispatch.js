const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const dispatchController = require('../controller/dispatchController');
const safeJsonStringify = require('safe-json-stringify');
router.post('/',(req, res) => {
    res.json({"msg" : "Welcome to API"});
});

router.post('/dispatch',/* verifyToken, */async (req,res)=>{
    const dataFromController = await dispatchController.createDispatch(req,res);
    //res.json(safeJsonStringify(dataFromController));
});

router.get('/dispatch',async (req,res)=>{
   let resultDataArray={};

   let getAllDispatchesDataArray = await dispatchController.getAllDispatch(req,res);

   try {
    getAllDispatchesDataArray.forEach(async function(getAllDispatchesData, _index, arr) {
       /*  let sourceName = await dispatchController.getSourceName(getAllDispatchesData['sourceId'],res);
        let destinationName = await dispatchController.getSourceName(getAllDispatchesData['destinationId'],res);
        let transporterName = await dispatchController.getTransporterName(getAllDispatchesData['transporterMasterId'],res);
        let driverName = await dispatchController.getDriverName(getAllDispatchesData['driverMasterId'],res);
        let routeName = await dispatchController.getRouteName(getAllDispatchesData['routeMasterId'],res);
        let tripStatus = await dispatchController.getTripStatusName(getAllDispatchesData['tripStatusMasterId'],res);
        let skuCode = await dispatchController.getSkuCode(getAllDispatchesData['SkuMasterId'],res);
        let vehicleType = await dispatchController.getVehicleTypeByName(getAllDispatchesData['vehicleTypeMasterId'],res);  */
    
        //resultDataArray['sourceName'] = await dispatchController.getSourceName(getAllDispatchesData['sourceId'],res);//sourceName;
       /*  resultDataArray['destinationName'] = await destinationName;
        resultDataArray['transporterName'] = await transporterName;
        resultDataArray['driverName'] = await driverName;
        resultDataArray['routeName'] = await routeName;
        resultDataArray['tripStatus'] = await tripStatus;
        resultDataArray['skuCode'] = await skuCode;
        resultDataArray['vehicleType'] = await  vehicleType; */
    
    });
       
   } catch (error) {
       
   }
   

   /* Promises testing starts here */
        var bar = new Promise((resolve, reject) => {
            getAllDispatchesDataArray.forEach(async (getAllDispatchesData, index, array) => {
                //console.log(value);
               
                if (index === array.length -1){
                     resultDataArray['sourceName'] =  await dispatchController.getSourceName(getAllDispatchesDataArray[index]['sourceId'],res);//sourceName;
                    resultDataArray['destinationName'] =  await dispatchController.getSourceName(getAllDispatchesDataArray[index]['destinationId'],res);;
                    resultDataArray['transporterName'] =  await dispatchController.getTransporterName(getAllDispatchesDataArray[index]['transporterMasterId'],res);;
                    resultDataArray['driverName'] = await dispatchController.getDriverName(getAllDispatchesDataArray[index]['driverMasterId'],res);;
                    resultDataArray['routeName'] = await dispatchController.getRouteName(getAllDispatchesDataArray[index]['routeMasterId'],res);;
                    resultDataArray['tripStatus'] = await  dispatchController.getTripStatusName(getAllDispatchesDataArray[index]['tripStatusMasterId'],res);;
                    resultDataArray['skuCode'] = await dispatchController.getSkuCode(getAllDispatchesDataArray[index]['SkuMasterId'],res);;
                    resultDataArray['vehicleType'] = await  dispatchController.getVehicleTypeByName(getAllDispatchesDataArray[index]['vehicleTypeMasterId'],res); ;
                    resolve();
                } else {
                   // resultDataArray['sourceName'] =  dispatchController.getSourceName(getAllDispatchesData['sourceId'],res);//sourceName;
                 //   resultDataArray['destinationName'] =   dispatchController.getSourceName(getAllDispatchesData['destinationId'],res);;
                 //   resultDataArray['transporterName'] =  dispatchController.getTransporterName(getAllDispatchesData['transporterMasterId'],res);;
                 //   resultDataArray['driverName'] =  dispatchController.getDriverName(getAllDispatchesData['driverMasterId'],res);;
                 //   resultDataArray['routeName'] =  dispatchController.getRouteName(getAllDispatchesData['routeMasterId'],res);;
                   // resultDataArray['tripStatus'] =  dispatchController.getTripStatusName(getAllDispatchesData['tripStatusMasterId'],res);;
                //    resultDataArray['skuCode'] =  dispatchController.getSkuCode(getAllDispatchesData['SkuMasterId'],res);;
                //    resultDataArray['vehicleType'] =   dispatchController.getVehicleTypeByName(getAllDispatchesData['vehicleTypeMasterId'],res); ;
                   
                } 
            });
        });

        bar.then(() => {
           // console.log('All done!: '+resultDataArray);
           return res.json({
            'data':resultDataArray
           });
        });
   /* Promises testing ends here */

/*    return res.json({
    'data':resultDataArray
   }); */
    //console.log("Whole array data is: "+resultDataArray);
   
});

router.get('/dispatch/:id',async(req,res)=>{
    //dispatchController.getDispatchById(req,res);
    return res.json({
        'message':'I am in GET with specific id'
    });
});

router.put('/dispatch/:id',async(req,res)=>{
    return res.json({
        'message':'I am in PUT'
    });
});

router.delete('/dispatch/:id',async(req,res)=>{
    return res.json({
        'message':'I am in DELETE'
    });
});

module.exports = router;